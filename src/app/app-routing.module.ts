import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeListComponent } from './emp/employee-list/employee-list.component';


export const routes: Routes = [
  {
    path: '',
    redirectTo: 'employeesList',
    pathMatch: 'full'
  },
  {
    path: 'employeesList',
    component: EmployeeListComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
