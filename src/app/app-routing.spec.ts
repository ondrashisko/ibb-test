import {Location} from "@angular/common";
import {TestBed, fakeAsync, tick} from '@angular/core/testing';
import {RouterTestingModule} from "@angular/router/testing";
import {Router} from "@angular/router";
import { EmployeeListComponent } from './emp/employee-list/employee-list.component';
import { TestingModule } from './testing/testing.module';
import { routes } from './app-routing.module'

describe('Router: App', () => {
    let location: Location;
    let router: Router;
    let fixture;
  beforeEach(() => {
    TestBed.configureTestingModule({
        imports:[
            TestingModule,
            RouterTestingModule.withRoutes(routes)
        ],
        declarations: [ 
            EmployeeListComponent,

      ]
    });
    router = TestBed.get(Router); 
    location = TestBed.get(Location);

    fixture = TestBed.createComponent(EmployeeListComponent);
    router.initialNavigation();
  });

  it(' initial route should navigate to employees list', fakeAsync(() => { (1)
    router.navigate(['']);
    tick(); 
    expect(location.path()).toBe('/employeesList'); (4)
  }));
  
});