import {
  async,
  fakeAsync,
  ComponentFixture,
  TestBed

} from '@angular/core/testing';

import { EmployeeFormComponent } from './employee-form.component';

import { ReactiveFormsModule, FormGroup } from '@angular/forms';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { TestingModule } from '../../testing/testing.module';
import { validEmployee, dialogMock, blankEmployee, positionsMock } from '../../mocks';
import { EmployeeService } from '../employee.service';

describe('EmployeeFormComponent', () => {
  let component: EmployeeFormComponent;
  let fixture: ComponentFixture<EmployeeFormComponent>;
  let empForm: FormGroup;
  let service: EmployeeService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        TestingModule,
      ],
      declarations: [EmployeeFormComponent],
      providers: [
        { provide: MatDialogRef, useValue: dialogMock },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeFormComponent);
    component = fixture.componentInstance;
    empForm = component.employeeForm;
    service = TestBed.inject(EmployeeService);

    fixture.detectChanges();
  });

  function poluteForm(first, last, position, date) {
    empForm.controls['firstName'].setValue(first);
    empForm.controls['lastName'].setValue(last);
    empForm.controls['position'].setValue(position);
    empForm.controls['birthdate'].setValue(date);
  }

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form value should update from form changes', fakeAsync(() => {
    poluteForm(validEmployee.firstName, validEmployee.lastName, validEmployee.position, validEmployee.birthdate);
    expect(empForm.value).toEqual(validEmployee);
  }));

  it('should update model on submit', fakeAsync(() => {
    poluteForm(validEmployee.firstName, validEmployee.lastName, validEmployee.position, validEmployee.birthdate);
    component.onSubmit();
    expect(component.employee).toEqual(validEmployee);
  }));

  it('employeeForm should be invalid', () => {
    poluteForm(blankEmployee.firstName, blankEmployee.lastName, blankEmployee.position, blankEmployee.birthdate);
    expect(empForm.valid).toBeFalsy()
  });

  it('employeeForm should be valid',() => {
    poluteForm(validEmployee.firstName, validEmployee.lastName, validEmployee.position, validEmployee.birthdate);
    expect(empForm.valid).toBeTruthy();
  })

  it('should get positions',()=>{
    service.getPositionsMock().subscribe(data=>{
      expect(data == positionsMock).toBeTruthy()
    })
  })

});
