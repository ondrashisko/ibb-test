import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog'
import { Employee } from '../employee.class';
import { EmployeeService } from '../employee.service';


@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.sass']
})
export class EmployeeFormComponent implements OnInit {
  employee: Employee;
  employeeForm: FormGroup;
  positions: Array<any>;
  edit: boolean;

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private employeeService: EmployeeService
  ) {
    // test if editing employee or creating new one
    data.employee ? this.employee = data.employee : null;
    this.createForm();
    this.getPositions()
  }

  ngOnInit(): void {
    this.setEmpForEdit(this.employee)
  }

  createForm() {
    this.employeeForm = this.fb.group({
      id: [],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      position: ['', Validators.required],
      birthdate: ['',Validators.required]
    })
  }
  setEmpForEdit(e: Employee) {
    if (e) {
      this.employeeForm.setValue(e)
      this.edit = true
    }
  }
  getPositions() {
    this.employeeService.getPositions()
      .subscribe(data => {
        let p: any = data
        this.positions = p.positions
      })
  }

  onSubmit() {
    this.employee = this.employeeForm.value
    this.dialogRef.close(this.employee)
  }

  deleteEmployee() {
    if(confirm("Are you sure to delete "+this.employee.lastName)) {
      this.employeeService.deleteEmployee(this.employee.id)
      this.dialogRef.close()
    }
  }

}
