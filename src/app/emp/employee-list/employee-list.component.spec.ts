import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeListComponent } from './employee-list.component';
import { TestingModule } from '../../testing/testing.module';

import { DebugElement } from '@angular/core'
import { employees as employeesMock, dialogMock } from '../../mocks';
import { MatDialog } from '@angular/material/dialog';
import { EmployeeFormComponent } from '../employee-form/employee-form.component';

describe('EmployeeListComponent', () => {
  let component: EmployeeListComponent;
  let fixture: ComponentFixture<EmployeeListComponent>;
  let de: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[TestingModule],
      declarations: [ EmployeeListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeListComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
    component.employees = employeesMock ;
    component.filteredEmployees = employeesMock;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should Search the employees',()=>{ 
    component.searchEmployees('la') 
    expect(component.filteredEmployees).toEqual(employeesMock)
  })

});
