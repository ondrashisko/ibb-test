import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog'
import { EmployeeFormComponent } from '../employee-form/employee-form.component';
import { Employee } from '../employee.class';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.sass']
})
export class EmployeeListComponent implements OnInit {
  employees: Employee[]
  filteredEmployees: Employee[]

  constructor(
    public dialog: MatDialog,
    private employeeService: EmployeeService
  ) {
  }

  ngOnInit(): void {
    this.getEmployees()
  }

  getEmployees() {
    this.employeeService.getEmployees()
      .subscribe(data => {
        this.employees = data.map(e => {
          return {
            id: e.payload.doc.id,
            ...e.payload.doc.data() as any
          } as Employee
        })
        this.filteredEmployees = this.employees
      })
  }

  openEmployeeForm(e?: Employee, index?: number) {
    let dialogRef = this.dialog.open(
      EmployeeFormComponent, {
      width: 'auto',
      data: { employee: e ? e : null }
    })
    
    dialogRef.afterClosed()
      .subscribe(res => {
        if (res) {
          if (index != undefined) {
            // update employee
            let e: Employee = res
            e.id = this.employees[index].id
            this.employeeService.updateEmployee(e)
          }
          else {
            // create employee
            delete res.id
            this.employeeService.createEmployee(res)
          }
        }
      })

  }

  isEven(n) {
    return n % 2 == 0;
  }

  searchEmployees(query: string) {
    this.filteredEmployees = this.employees.filter(e => {
      return e.firstName.toLocaleLowerCase().includes(query) || e.lastName.toLocaleLowerCase().includes(query)
    })
  }

}
