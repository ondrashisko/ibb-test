import { EmployeeService } from './employee.service';
export class Employee {
    id?: string;
    firstName: string;
    lastName: string;
    position: string;
    birthdate: Date;
}

