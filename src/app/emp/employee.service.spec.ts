import { TestBed } from '@angular/core/testing';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';

import { EmployeeService } from './employee.service';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { TestingModule } from '../testing/testing.module';

describe('EmployeeService', () => {
  let service: EmployeeService;

  beforeEach(() => {TestBed.configureTestingModule({
    imports:[
      TestingModule
    ]
  });
    service = TestBed.inject(EmployeeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get positions',()=>{
    service.getPositionsMock().subscribe(data=>{
      expect(data.positions.length > 0).toBeTruthy()
    })
  })
});
