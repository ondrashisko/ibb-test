import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';
import { Employee } from './employee.class';
import { positionsMock } from '../mocks';
import { of } from 'rxjs'

const positionsApiUrl =  'http://ibillboard.com/api/positions';

@Injectable({
  providedIn: 'root'
})

export class EmployeeService {
  empRef = this.afs.collection('employees')

  constructor(
    private http: HttpClient,
    private afs: AngularFirestore
    ) { }
    
  getPositions(){
      return this.http.get(positionsApiUrl)
    }
    
  getPositionsMock(){
    return of( positionsMock )
  }

  createEmployee(e: Employee){
    let je = JSON.parse(JSON.stringify(e))
    return this.empRef.add(je)
  }

  getEmployees(){
    return this.empRef.snapshotChanges()
  }

  updateEmployee(e: Employee){
    let id = e.id;
    delete e.id
    return this.empRef.doc(id).update(e)
  }
  

  deleteEmployee(id: string){
    return this.empRef.doc(id).delete()
  }

}
