import { Employee } from './emp/employee.class';

export let validEmployee: Employee = {
    id: null,
    firstName: 'Otto',
    lastName: 'Lotr',
    position: 'Help Desk',
    birthdate: new Date(1988, 10, 25)
}

export let blankEmployee: Employee = {
    id: null,
    firstName: null,
    lastName: null,
    position: null,
    birthdate: null
}

export let dialogMock = {
    close: () => { }
};

export let employees: Employee[] = [
    {
        id: '1',
        firstName: 'Lara',
        lastName: 'Croft',
        position: 'Help Desk',
        birthdate: new Date(2000, 10, 20)
    }
]

export let positionsMock = {
    "positions":["full-stack developer","front-end developer","sw admin","help desk","scrum master","product manager"]
}
