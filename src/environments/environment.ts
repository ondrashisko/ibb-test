// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyC1oXclCNokvpB0WtEBwVLGCd-lbdsVwMI",
    authDomain: "ondrejbauer-aaa40.firebaseapp.com",
    databaseURL: "https://ondrejbauer-aaa40.firebaseio.com",
    projectId: "ondrejbauer-aaa40",
    storageBucket: "ondrejbauer-aaa40.appspot.com",
    messagingSenderId: "904218364746",
    appId: "1:904218364746:web:ea767412da2fe91c6f8fb3"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
